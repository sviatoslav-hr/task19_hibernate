USE ships;

INSERT INTO ship_class (name, type, country, guns_num, bore, displacement)
VALUES ('Bismarck', 'bb', 'Germany', 8, 15, 42000),
       ('Lowa', 'bb', 'USA', 9, 16, 46000),
       ('Kongo', 'bc', 'Japan', 8, 14, 32000),
       ('North Carolina', 'bb', 'USA', 12, 16, 37000),
       ('Renown', 'bc', 'GtBritain', 6, 15, 32000),
       ('Revenge', 'bb', 'GtBritain', 8, 15, 29000),
       ('Tennessee', 'bb', 'USA', 12, 14, 32000),
       ('Yamato', 'bb', 'Japan', 9, 18, 65000);

INSERT INTO ship (name, launch_year, class_id)
VALUES ('California', 1921, 7),
       ('Haruna', 1916, 3),
       ('Hiei', 1914, 3),
       ('lowa', 1943, 2),
       ('Kirishima', 1915, 3),
       ('Kongo', 1913, 3),
       ('Missouri', 1944, 2),
       ('Musashi', 1942, 8),
       ('New Jersey', 1943, 2),
       ('North Carolina', 1941, 4),
       ('Ramillies', 1917, 6),
       ('Renown', 1916, 5),
       ('Repulse', 1916, 5),
       ('Resolution', 1916, 5),
       ('Revenge', 1916, 6),
       ('Royal Oak', 1916, 6),
       ('Royal Sovereign', 1916, 6),
       ('South Dakota', 1941, 4),
       ('Tennessee', 1920, 7),
       ('Washington', 1941, 4),
       ('Wisconsin', 1944, 2),
       ('Yamato', 1941, 8);

INSERT INTO battle (name, date)
VALUES ('#Cuba62a', '1962-10-20'),
       ('#Cuba62b', '1962-10-25'),
       ('Guadalcanal', '1942-11-15'),
       ('North Atlantic', '1941-05-25'),
       ('North Cape', '1943-12-26'),
       ('Surigao Strait', '1944-10-25');

INSERT INTO outcome_result(name)
VALUES ('sunk'),
       ('damaged'),
       ('OK');

INSERT INTO outcome(ship_id, battle_id, result_id)
VALUES (1, 1, 1),
       (2, 1, 3),
       (3, 1, 2),
       (4, 2, 1),
       (5, 2, 2),
       (6, 2, 3),
       (7, 3, 3),
       (8, 4, 2),
       (9, 1, 3),
       (10, 5, 3),
       (11, 6, 2),
       (12, 4, 3),
       (13, 2, 3),
       (14, 1, 3),
       (15, 4, 2),
       (16, 5, 3);