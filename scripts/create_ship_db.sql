CREATE DATABASE IF NOT EXISTS ships;

USE ships;

CREATE TABLE IF NOT EXISTS ship_class
(
    id           INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name         VARCHAR(50)     NOT NULL,
    type         VARCHAR(2)      NOT NULL,
    country      VARCHAR(20)     NOT NULL,
    guns_num     INT,
    bore         FLOAT,    displacement int
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS ship
(
    id          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name        varchar(50)     NOT NULL,
    launch_year INT             NOT NULL,
    class_id    INT             NOT NULL
) ENGINE = InnoDB;

ALTER TABLE ship ADD FOREIGN KEY (class_id) REFERENCES ship_class(id);

CREATE TABLE IF NOT EXISTS battle
(
    id   INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(20)     NOT NULL,
    date DATETIME        NOT NULL
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS outcome_result
(
    id   INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(10)     NOT NULL
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS outcome
(
    ship_id   int,
    battle_id int,
    result_id int,
    PRIMARY KEY (ship_id, battle_id, result_id)
) ENGINE = InnoDB;

ALTER TABLE outcome ADD FOREIGN KEY (ship_id) REFERENCES ship(id);
ALTER TABLE outcome ADD FOREIGN KEY (battle_id) REFERENCES battle(id);
ALTER TABLE outcome ADD FOREIGN KEY (result_id) REFERENCES outcome_result(id);