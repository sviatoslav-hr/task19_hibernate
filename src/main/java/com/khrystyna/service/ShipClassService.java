package com.khrystyna.service;

import com.khrystyna.dao.GeneralDao;
import com.khrystyna.dao.ShipClassDaoImpl;
import com.khrystyna.entity.ShipClass;

public class ShipClassService extends GeneralService<ShipClass, Integer> {
    private static GeneralDao<ShipClass, Integer> shipClassDao;

    @Override
    GeneralDao<ShipClass, Integer> getEntityDao() {
        if (shipClassDao == null) {
            shipClassDao = new ShipClassDaoImpl();
        }
        return shipClassDao;
    }

}
