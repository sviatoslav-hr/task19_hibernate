package com.khrystyna.service;

import com.khrystyna.dao.GeneralDao;

import java.util.List;

public abstract class GeneralService<T, ID> {

    abstract GeneralDao<T, ID> getEntityDao();

    public void persist(T entity) {
        getEntityDao().openCurrentSessionWithTransaction();
        getEntityDao().persist(entity);
        getEntityDao().closeCurrentSessionWithTransaction();
    }

    public void update(T entity) {
        getEntityDao().openCurrentSession();
        getEntityDao().update(entity);
        getEntityDao().closeCurrentSession();
    }

    public T findById(ID id) {
        getEntityDao().openCurrentSession();
        T entity = getEntityDao().findById(id);
        getEntityDao().closeCurrentSession();
        return entity;
    }

    public void delete(ID id) {
        getEntityDao().openCurrentSessionWithTransaction();
        T entity = getEntityDao().findById(id);
        getEntityDao().delete(entity);
        getEntityDao().closeCurrentSessionWithTransaction();
    }

    public List<T> findAll() {
        getEntityDao().openCurrentSession();
        List<T> all = getEntityDao().findAll();
        getEntityDao().closeCurrentSession();
        return all;
    }

    public void deleteAll() {
        getEntityDao().openCurrentSessionWithTransaction();
        getEntityDao().deleteAll();
        getEntityDao().closeCurrentSessionWithTransaction();
    }
}
