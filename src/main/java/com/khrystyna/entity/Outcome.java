package com.khrystyna.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@IdClass(OutcomePK.class)
@Data
public class Outcome {
    @Id
    @Column(name = "ship_id")
    private Integer shipId;
    @Id
    @Column(name = "battle_id")
    private Integer battleId;
    @Id
    @Column(name = "result_id")
    private Integer resultId;
    @ManyToOne(targetEntity = Ship.class)
    @JoinColumn(name = "ship_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Ship ship;
    // fixed: MappingException: Repeated column in mapping for entity: com.khrystyna.entity.Outcome column: battle_id
    @ManyToOne(targetEntity = Battle.class)
    @JoinColumn(name = "battle_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Battle battle;
    @ManyToOne(targetEntity = OutcomeResult.class)
    @JoinColumn(name = "result_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private OutcomeResult outcomeResult;
}
