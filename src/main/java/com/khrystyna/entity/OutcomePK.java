package com.khrystyna.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Data
public class OutcomePK implements Serializable {
    @Column(name = "ship_id")
    @Id
    private Integer shipId;
    @Column(name = "battle_id")
    @Id
    private Integer battleId;
    @Column(name = "result_id")
    @Id
    private Integer resultId;
}
