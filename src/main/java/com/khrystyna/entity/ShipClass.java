package com.khrystyna.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "ship_class")
@Data
public class ShipClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "type")
    private String type;
    @Basic
    @Column(name = "country")
    private String country;
    @Basic
    @Column(name = "guns_num")
    private Integer gunsNum;
    @Basic
    @Column(name = "bore")
    private Double bore;
    @Basic
    @Column(name = "displacement")
    private Integer displacement;
    // fixed: Could not determine type for: java.util.Collection, at table: ship_class...
    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "shipClass", cascade = CascadeType.ALL)
    private Collection<Ship> ships;


    public Collection<Ship> getShips() {
        return ships;
    }
}
