package com.khrystyna.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
public class Ship {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "launch_year")
    private Integer launchYear;
    @OneToMany(targetEntity = Outcome.class, fetch = FetchType.LAZY, mappedBy = "ship", cascade = CascadeType.ALL)
    private Collection<Outcome> outcomes;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_id", referencedColumnName = "id", nullable = false)
    private ShipClass shipClass;
}
