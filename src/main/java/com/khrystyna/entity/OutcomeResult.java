package com.khrystyna.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "outcome_result", schema = "ships")
@Data
public class OutcomeResult {
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "name")
    private String name;
}
