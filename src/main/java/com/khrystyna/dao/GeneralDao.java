package com.khrystyna.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public abstract class GeneralDao<T, ID> {
    Session currentSession;

    private Transaction currentTransaction;

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public void openCurrentSessionWithTransaction() {
        currentTransaction = openCurrentSession().beginTransaction();
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction() {
        currentTransaction.commit();
        closeCurrentSession();
    }

    private static SessionFactory getSessionFactory() {
        return new Configuration().configure().buildSessionFactory();
    }

    public void persist(T entity) {
        currentSession.persist(entity);
    }

    public void update(T entity) {
        currentSession.save(entity);
    }

    public abstract T findById(ID id);

    public void delete(T entity) {
        currentSession.delete(entity);
    }

    public abstract List<T> findAll();

    public void deleteAll() {
        for (T entity : findAll()) {
            delete(entity);
        }
    }

}
