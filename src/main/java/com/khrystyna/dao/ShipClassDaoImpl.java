package com.khrystyna.dao;

import com.khrystyna.entity.ShipClass;

import java.util.List;

public class ShipClassDaoImpl extends GeneralDao<ShipClass, Integer> {

    @Override
    public ShipClass findById(Integer id) {
        return currentSession.get(ShipClass.class, id);
    }
    @Override
    @SuppressWarnings("unchecked")
    public List<ShipClass> findAll() {
        return currentSession.createQuery("from ShipClass").list();
    }
}
