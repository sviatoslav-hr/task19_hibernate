package com.khrystyna;

import com.khrystyna.entity.ShipClass;
import com.khrystyna.service.ShipClassService;

import java.util.Collection;

public class Application {
    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        ShipClassService service = new ShipClassService();

        printAll(service.findAll());

        printLine();

        printAll(service.findAll());

    }

    private static void printAll(Collection collection) {
        for (Object o : collection) {
            System.out.println(o);
        }
    }

    private static void printLine() {
        System.out.println("________________________________________________");
    }

    private static ShipClass getNew() {
        ShipClass shipClass = new ShipClass();
        shipClass.setName("New One");
        shipClass.setType("gb");
        shipClass.setCountry("New Zealand");
        shipClass.setGunsNum(34);
        shipClass.setBore(2.3);
        shipClass.setDisplacement(6);
        return shipClass;
    }
}
